#include <iostream>
#include <vector>

using namespace std;

/*
 * Sum elements in given vector
 */
int sum(int beg, int end, vector<int> &V) {
    int sum = 0;
    for(;beg != end; beg ++) {
        sum += V[beg];
    }
    return sum;
}

/*
 * Find 2 elements dividing array into 3 subarrays which sum is even
 */
bool solution(vector<int> &A) {
    int pre_sum, post_sum = 0;
    int in_sum = sum(0, A.size(), A);

    for (int beg = 0; beg < A.size(); beg++) {
        in_sum -= A[beg];
        int prepared_sum = in_sum;

        for (int end = A.size()-1; end != beg; end--) {
            post_sum += A[end+1];
            prepared_sum -= A[end];

            if (pre_sum == prepared_sum && prepared_sum == post_sum) {
                return true;
            }
        }
        pre_sum += A[beg];
        post_sum = 0;
    }
    return false;
}

int main() {
    vector<int> a, b, c;
    // 1 3 4 2 2 2 1 1 2
    a.push_back(1);
    a.push_back(3);
    a.push_back(4);
    a.push_back(2);
    a.push_back(2);
    a.push_back(2);
    a.push_back(1);
    a.push_back(1);
    a.push_back(2);

    // 1,2,1,2,1,2
    for (int i = 0; i < 10000; i++) {
        b.push_back(1);
        b.push_back(2);
    }

    // 1 1 1 1 1 1
    c.push_back(1);
    c.push_back(1);
    c.push_back(1);
    c.push_back(1);
    c.push_back(1);
    c.push_back(1);

    cout << solution(a); // 1
    cout << solution(b); // 1
    cout << solution(c); // 0

    return 0;
}